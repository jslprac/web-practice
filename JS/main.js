


// IIFE : pollute the global scope as less as possible
(function() {
	/* global google */

	function myMap() {
		var mapProp = {
			center: new google.maps.LatLng(37.540881, 127.079689),
			zoom: 17,
		};
		new google.maps.Map(document.getElementById("googleMap"), mapProp);
		// new google.maps.Map(document.getElementById("googleMap2"),mapProp);
	}

	window.myMap = myMap;
}());
